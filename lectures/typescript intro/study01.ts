
// typescript를 왜 쓰냐? 타입으로 인한 에러를 방지하기 위함 (틀린 거 고쳐주는 문법)
let _name: string = "kim";
let str_num: string | number = 123;
type type_my = string | number;
let testType: type_my = "ss223";

// 함수의 타입 지정(인자값, 리턴값에 타입을 지정)
function TypeFuncTest(x:number) : number{
    return x * 2
}

console.log(TypeFuncTest(3))

// 타입을 json = {key : value} 형식으로 지정
type Member = {
    [key : string] : string // key를 문자로만 받고 value도 문자로만 받음
}
let john :Member = { name : 'kim' }


class User {
    name:string;
    constructor(name:string){
        this.name = name;
    }
}