// 자주 사용되는 유틸리티 타입
// keyof
interface User09 {
  id: number;
  name: string;
  age: number;
  gender?: "m" | "f";
}

type UserKey = keyof User09; // User09의 key값들을 유니온 형태로 받을 수 있음
const uk: UserKey = "name"; // 'id' | 'name' | 'age' | 'gender' 중 하나만 받을 수 있음

// Partial<T> : 부분만 정의해도 사용할 수 있게끔 해줌
let admin: Partial<User09> = {
  id: 1,
  name: "a",
  age: 10,
};
console.log(admin.name);

// Required<T> : 해당 자료형 타입으로 감싸면 모든 필드가 필수 정의해야되는 조건으로 변동됨
let admin_2: Required<User09> = {
  id: 1,
  name: "a",
  age: 10,
  gender: "m", // 이 타입은 gender? 이여도 정의해줘야 함
};
console.log(admin_2.name);

// ReadOnly<T> 읽기전용으로 변환시키는 Type
// 처음에 할당만 가능하고 후에 수정은 불가
// Partial<T> : 부분만 정의해도 사용할 수 있게끔 해줌
let admin_3: Readonly<User09> = {
  id: 1,
  name: "a",
  age: 10,
};
//admin_3.name = "dd" // 변환 불가
console.log(admin_3.name);

// Record<K,T>
// 특정 값만 key와 value로 세팅하려는 경우 사용
type Grade = '1' | '2' | '3' | '4';
type ScoreValue = 'A' | 'B' | 'C' | 'D';

// 위의 정의된 타입만 쓸 수 있음
const score: Record<Grade, ScoreValue> = {
    1: 'A',
    2: 'B',
    3: 'C', 
    4: 'D'
}

function isValid(user: User09): object {
    const result: Partial<Record<keyof User09, boolean>> = {
        id: user.id > 0,
        name: user.name !== "",
        age: user.age > 0,
    }
    return result;
}

// Pick<T,K> :: value로 정의된 것만 쓰겠다는 의미
let admin_5: Pick<User09, "id" | "name"> = {
    id: 1,
    name: "a"
};


// Omit<K,T> :: value로 정의된 거 외의 것만 쓰겠다는 의미
let admin_6: Omit<User09, "age" | "gender"> = {
    id: 1,
    name: "a"
};

// Exclude<T1,T2> : 타입을 비교해서 제외시키는 방법
type T1 = string | number | boolean
type T2 = Exclude<T1, number | string> // T1에서 제외할 타입을 정의 => boolean만 남음

// NonNullable<Type> : 타입을 비교해서 null, undefined가 아닌 것만 타입으로 정의
type T3 = string | null | undefined | void;
type T4 = NonNullable<T3> // string | void만 남음

