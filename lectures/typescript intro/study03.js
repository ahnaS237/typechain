let obj;
obj = { name: 'Amy', age: 18 };
// 최초 변수 정의 (초기화)
let user = {
    name: 'xx',
    age: 30,
    birthYear: 2000,
    1: 'A',
    //2 : 'a' // Score에 있는 값만 입력 가능
};
// 이제 type이 정의된 property를 사용할 수 있음
user.age = 10;
user.gender = "male";
// 위 타입을 사용한 함수 정의
const add = function (x, y) {
    return x * y;
};
add(10, 20);
const checkAdult = function (age) {
    return age > 19;
};
checkAdult(18); // true
// 클래스 정의시 상속받은 상태로 사용 가능
class Bmw {
    constructor(c) {
        this.wheels = 4;
        this.color = c;
    }
    // ?를 붙이면 반드시 사용할 필요는 없음
    start() {
        //console.log('go...!!')
    }
}
const bmw = new Bmw('green');
//console.log(bmw);
bmw.start(); // go...!!
const benz = {
    door: 5,
    stop() {
        //console.log('stop!')
    },
    // Car까지 다 정의해줘야 함
    color: 'black',
    wheels: 4,
    // start() {
    //     console.log('go?')
    // }
};
