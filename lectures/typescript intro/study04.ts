function hello(age: number | undefined, name?: string) : string{
    if(age !== undefined){
        return `Hello, ${name}. You are ${age}.`;
    } else {
        return `Hello, ${name || "world"}`;
    }
}

const result = hello(30, "Sam");
const result2 = hello(undefined, "sam");

function add2(...nums: number[]): number{
    return nums.reduce((result, num) => result + num, 0);
}

add2(1,2,3);
add2(1,2,3,4,5,6,7,8,9,10);

// this
interface User2 {
    name: string,
    age?: number
}

const Sam: User2 = {name: 'Sam'}

// 여러개의 매개변수를 this와 같이 사용할시 this를 제일 먼저 정의해줘야 한다.
function showName(this: User, age: number, gender :'m'|'f'){
    console.log(this.name, age, gender);
}

const a = showName.bind(Sam) // 매개변수로 Sam을 전달 == this에 bind(담기)
a(30, 'm');

// function은 같은 이름으로 여러개 사용 가능
function join(name: string, age: string): string;
function join(name: string, age: number): User2;
function join(name: string, age: number | string): User2 | string {
    if(typeof age === "number"){
        return {
            name,
            age
        };
    } else {
        return "나이는 숫자로 입력하세요."
    }
}

const sam: User2 = join("Sam", 30)
const jane: string = join("Jane", "30");