let obj:object;
obj = { name : 'Amy', age : 18}
//console.log(obj.name); // object라는 타입에서 name property를 정의해놓은 게 아니라서 오류가 남

// 위의 대안으로 쓰인 게 interface
type Score = 'A' | 'B' | 'C' | 'F';
interface User {
    name : string;
    age : number;
    gender? : string; 
    // 인터페이스에 type이 추가될 때마다 반드시 정의해줘야 에러가 안나는 데 
    // 사용하지 않을 시 ?를 쓰면 케이스별로 gender는 사용할 수도 안할 수도 있다는 뜻
    readonly birthYear : number;
    [grade: number] : Score // grade 대신 아무 변수 사용 가능(grade에 의미가 있는 건 아님)
}

// 최초 변수 정의 (초기화)
let user : User = {
    name : 'xx',
    age : 30,
    birthYear : 2000,
    1 : 'A',
    //2 : 'a' // Score에 있는 값만 입력 가능
}

// 이제 type이 정의된 property를 사용할 수 있음
user.age = 10
user.gender = "male"
//user.birthYear = 1999 // 위에서 readonly로 초기화할 때만 한번 정의할 수 있게 해놨음으로 변경 불가 (오류)

// 인터페이스로 인자값도 한번에 type 정의 가능
// 함수형 타입 정의 가능
interface Add {
    (num1 : number, num2 : number): number;
}

// 위 타입을 사용한 함수 정의
const add : Add = function(x,y) {
    return x * y
}
add(10, 20);

// 함수 타입 정의
interface isAdult {
    (age:number) : boolean;
}

const checkAdult:isAdult = function(age){
    return age > 19;
} 

checkAdult(18); // true

// implements 사용 [상속]
interface Car {
    color : string;
    wheels: number;
    start?(): void; // 해당 인터페이스 상속시 해당 함수를 사용할 수 있음을 의미
}

interface Toy {
    name : string;
}

// 클래스 정의시 상속받은 상태로 사용 가능
class Bmw implements Car {
    color;
    wheels = 4;
    constructor(c:string){
        this.color = c;
    }
    // ?를 붙이면 반드시 사용할 필요는 없음
    start(){
        //console.log('go...!!')
    }
}

const bmw = new Bmw('green');
//console.log(bmw);

bmw.start(); // go...!!


// extends [확장]
interface Benz extends Car {
    door : number;
    stop() : void;
}
 
const benz : Benz = {
    door : 5,
    stop(){
        //console.log('stop!')
    },
    // Car까지 다 정의해줘야 함
    color : 'black',
    wheels : 4,
    // start() {
    //     console.log('go?')
    // }
}

// 확장은 여러개 가능 ==> 이렇게 ToyCar를 정의 가능
interface ToyCar extends Toy, Car {
    price : number
}