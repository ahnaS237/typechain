const uk = "name"; // 'id' | 'name' | 'age' | 'gender' 중 하나만 받을 수 있음
// Partial<T> : 부분만 정의해도 사용할 수 있게끔 해줌
let admin = {
    id: 1,
    name: "a",
    age: 10,
};
console.log(admin.name);
// Required<T> : 해당 자료형 타입으로 감싸면 모든 필드가 필수 정의해야되는 조건으로 변동됨
let admin_2 = {
    id: 1,
    name: "a",
    age: 10,
    gender: "m", // 이 타입은 gender? 이여도 정의해줘야 함
};
console.log(admin_2.name);
// ReadOnly<T> 읽기전용으로 변환시키는 Type
// 처음에 할당만 가능하고 후에 수정은 불가
// Partial<T> : 부분만 정의해도 사용할 수 있게끔 해줌
let admin_3 = {
    id: 1,
    name: "a",
    age: 10,
};
//admin_3.name = "dd" // 변환 불가
console.log(admin_3.name);
// 위의 정의된 타입만 쓸 수 있음
const score = {
    1: 'A',
    2: 'B',
    3: 'C',
    4: 'D'
};
function isValid(user) {
    const result = {
        id: user.id > 0,
        name: user.name !== "",
        age: user.age > 0,
    };
    return result;
}
// Pick<T,K> :: value로 정의된 것만 쓰겠다는 의미
let admin_5 = {
    id: 1,
    name: "a"
};
// Omit<K,T> :: value로 정의된 거 외의 것만 쓰겠다는 의미
let admin_6 = {
    id: 1,
    name: "a"
};
