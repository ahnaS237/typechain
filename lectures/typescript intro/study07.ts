class Car7 {
    //color: string; // 접근제한자나 readonly를 써주면 정의를 안해줘도 된다! 
    // 접근 제한자(Access modifier) - public, private, protected
    constructor(readonly color: string){
        this.color = color;
    }
    start(){
        console.log("start")
    }
}

const bmw7 = new Car7("red") 
console.log(bmw7.color)

// 접근 제한자(Access modifier) - public, private, protected
// public - 자식 클래스, 클래스 인스턴스 모두 접근 가능
// protected - 자식 클래스에서 접근 가능
// private - 해당 클래스 내부에서만 접근 가능 (class 인스턴스[클래스명.파라미터] 접근 불가)
class Car8 {
    //public name: string = "car"
    //#name: string = "car"; // (# = private) :: ECMAScript 2015 이상만 사용 가능
    //protected name: string = "car";
    readonly name : string = "car"; // 이렇게 하면 아래ㅔ서 접근은 가능해서 수정은 불가
    static wheels = 4;
    color: string;
    constructor(color: string, name){
        this.color = color;
        this.name = name; // readonly여도 이렇게 해주면 수정이 가능
    }
    start() {
        console.log("start")
        console.log(this.name);
        console.log(Car8.wheels) // static은 정적변수로 클래스명으로 접근해서 사용 가능
    }
}

class Bmw8 extends Car8 {
    constructor(color: string, name){
        super(color, name);
    }
    public showName() {
        //console.log(this.name); // name이 public일때만 가능
        console.log(Car8.wheels)
    }
}

//const z4 = new Bmw8("black");
const z4 = new Bmw8("black", "zzz4");
//console.log(z4.name) // protected는 클래스 내부 변수로 접근 불가
console.log(z4.name) // readonly일 때 조회는 가능
//z4.name = "dddd" // readonly시 인스턴스 접근으로 수정은 불가!! 대신, 클래스 선언시 파라미터로 수정 가능
console.log(Car8.wheels)

// 추상 클래스
abstract class Car9 {
    color: string;
    constructor(color: string){
        this.color = color;
    }
    start() {
        console.log('start');
    }
    // 추상 클래스 내부의 추상 메소드는 반드시 상속 받는 클래스에서 정의를 해줘야 된다
    abstract doSomething(): void
}

class Bmw9 extends Car9{
    constructor(color: string){
        super(color);
    }
    // 구체적 정의가 반드시 필요
    doSomething(): void {
        alert(9)
    }
}