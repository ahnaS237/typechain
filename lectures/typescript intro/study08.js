// generic
// 매개변수가 변경될때마다 모든 인자 타입을 union으로 추가해줘야 함
// function getSize(arr: number[] | string[] | boolean[] | object[]): number {
//   return arr.length;
// }
// 모든 타입을 받을 수 있음(정의할 수 있음)
function getSize(arr) {
    return arr.length;
}
const arr1 = [1, 2, 3];
getSize(arr1); // 3
const arr2 = ["a", "b", "c"];
getSize(arr2); // 3
const arr3 = [false, true, true];
getSize(arr3); // 3
const arr4 = [{}, {}, { name: "Tim" }];
getSize(arr4); // 3
// 제너릭 타입 정의 해야됨
const m1 = {
    name: "s21",
    price: 1000,
    option: {
        color: "red",
        coupon: false,
    },
};
// 제너릭 타입 정의 해야됨
const m2 = {
    name: "s21",
    price: 1000,
    option: "good",
};
let user_1 = { name: "a", age: 10 };
let car = { name: "bmw", color: "red" };
let book = { price: 3000 };
// { name: string } type만 받겠다는 함수
function showName_10(data) {
    return data.name;
}
showName_10(user_1);
showName_10(car);
//showName_10(book) // name이 업거나 string이 아니면 에러가 발생
