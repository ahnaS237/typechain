// 타입스크립트를 왜 쓰는가? 2번째 예시
// forEach와 같은 타입에 따른 하위 함수를 사용할 수 있는 문법의 경우
// 반드시 호출하는 부분에서 자료형을 지켜야 한다 
function showItems(arr) {
    arr.forEach((item) => {
        //console.log(item)
    });
}
showItems([1, 2, 3]);
let b;
b = ['z', 1];
//b= [1, 's']
b[0].toLowerCase();
//b[1].toLowerCase();
// void
function sayHello() {
    //console.log('hello')
}
// never : 에러를 반환하거나 영원히 끝나지 않는 함수 호출시의 자료형으로 사용
function showError() {
    throw new Error();
}
function infLoop() {
    while (true) {
        // do something
    }
}
// enum
// value 타입을 정의
var Os;
(function (Os) {
    Os[Os["Window"] = 3] = "Window";
    Os[Os["Ios"] = 10] = "Ios";
    Os[Os["Android"] = 11] = "Android";
})(Os || (Os = {}));
//console.log(Os[10]) // Ios
let myOs; // Os에 등록된 명칭만 사용 가능
myOs = Os.Window; // 3
// null, undefined
let _a = null;
let _b = undefined;
