// Intersection Types

interface Car5 {
    name: string;
    start(): void;
}

interface Toy {
    name: string,
    color: string,
    price: number
}

const toyCar2: Toy & Car5 = { // & 두개 인터페이스의 모든 속성을 사용하겠다는 의미
    name: "타요",
    start(){},
    color: "blue",
    price: 1000
}