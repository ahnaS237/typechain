// generic

// 매개변수가 변경될때마다 모든 인자 타입을 union으로 추가해줘야 함
// function getSize(arr: number[] | string[] | boolean[] | object[]): number {
//   return arr.length;
// }

// 모든 타입을 받을 수 있음(정의할 수 있음)
function getSize<T>(arr: T[]): number {
  return arr.length;
}

const arr1 = [1, 2, 3];
getSize<number>(arr1); // 3

const arr2 = ["a", "b", "c"];
getSize<string>(arr2); // 3

const arr3 = [false, true, true];
getSize<boolean>(arr3); // 3

const arr4 = [{}, {}, { name: "Tim" }];
getSize(arr4); // 3

interface Mobile8<T> {
  name: string;
  price: number;
  option: T; // 타입을 받아서 지정하는 부분
}

// 제너릭 타입 정의 해야됨
const m1: Mobile8<{ color: string; coupon: boolean }> = {
  name: "s21",
  price: 1000,
  option: {
    color: "red",
    coupon: false,
  },
};

// 제너릭 타입 정의 해야됨
const m2: Mobile8<string> = {
  name: "s21",
  price: 1000,
  option: "good",
};

interface User9 {
  name: string;
  age: number;
}

interface Car10 {
  name: string;
  color: string;
}

interface Book9 {
  price: number;
}

let user_1: User9 = { name: "a", age: 10 };
let car: Car10 = { name: "bmw", color: "red" };
let book: Book9 = { price: 3000 };

// { name: string } type만 받겠다는 함수
function showName_10<T extends { name: string }>(data: T): string {
  return data.name;
}

showName_10(user_1)
showName_10(car)
//showName_10(book) // name이 업거나 string이 아니면 에러가 발생