function hello(age, name) {
    if (age !== undefined) {
        return `Hello, ${name}. You are ${age}.`;
    }
    else {
        return `Hello, ${name || "world"}`;
    }
}
const result = hello(30, "Sam");
const result2 = hello(undefined, "sam");
function add2(...nums) {
    return nums.reduce((result, num) => result + num, 0);
}
add2(1, 2, 3);
add2(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
const Sam = { name: 'Sam' };
// 여러개의 매개변수를 this와 같이 사용할시 this를 제일 먼저 정의해줘야 한다.
function showName(age, gender) {
    console.log(this.name, age, gender);
}
const a = showName.bind(Sam); // 매개변수로 Sam을 전달 == this에 bind(담기)
a(30, 'm');
function join(name, age) {
    if (typeof age === "number") {
        return {
            name,
            age
        };
    }
    else {
        return "나이는 숫자로 입력하세요.";
    }
}
const sam = join("Sam", 30);
const jane = join("Jane", "30");
