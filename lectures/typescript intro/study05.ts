// 리터럴 타입

const userName1 = "Bob"; // Bob외의 값을 가질 수 없음
let userName2: string | number = "Tom" // string 타입 아무거나 가능 (위보다 폭넓음)
userName2 = 3; //: string | number 해줘야 가능

interface User3 {
    name: string,
    job: string
}

const user3: User3 = {
    name: "Bob",
    job: "developer"
}

interface HighSchoolStudent {
    name: number | string; // union type
    grade: 1 | 2 | 3;
}

// Union Type
interface Car2 {
    name?: "car";
    color: string;
    start(): void;
} 

interface Mobile2 {
    name: "Mobile";
    color: string;
    call(): void;
}

function getGift(gift?: Car2 | Mobile2){
    console.log(gift.color);
    // gift.start(); 
    // ->  union type임으로 둘다 start가 없는 이상, 
    // 함수가 정의되지 않은 케이스에 대해서 오류로 처리
    if(gift.name === "car"){
        gift.start(); // 인자값에 name : "car"가 있어야 실행된다
    } else {
        gift.call();
    }
}

const car2 : Car2 = {
    name: "car",
    color: "blue",
    start: function () {
        console.log(111)
    }
}

getGift(car2);