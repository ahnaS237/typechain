// typescript를 왜 쓰냐? 타입으로 인한 에러를 방지하기 위함 (틀린 거 고쳐주는 문법)
let _name = "kim";
let str_num = 123;
let testType = "ss223";
// 함수의 타입 지정(인자값, 리턴값에 타입을 지정)
function TypeFuncTest(x) {
    return x * 2;
}
console.log(TypeFuncTest(3));
let john = { name: 'kim' };
class User {
    constructor(name) {
        this.name = name;
    }
}
