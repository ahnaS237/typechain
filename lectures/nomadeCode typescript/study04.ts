abstract class User04 {
    constructor(
        protected firstName: string,
        protected lastName: string,
    ){}
    abstract sayHi(name: string): string
    abstract fullName(): string
}

class Player04 extends User04 {
    fullName(): string {
        return `${this.firstName} ${this.lastName}`
    }
    sayHi(name: string): string {
        return `hello, ${name} my Name is ${this.fullName()}`
    }
}

// transfer class to interface : 코드가 짧아진다는 장점이 있지만 property 접근이 javascript와 다른 코드이다
interface User05 {
    firstName : string,
    lastName : string,
    sayHi(name: string): string,
    fullName(): string
}

// 클래스에 특정 메소드나 property를 상속
class Player05 implements User05 {
    constructor(
        public firstName: string,
        public lastName: string
    ){}
    fullName(): string {
        return `${this.firstName} ${this.lastName}`
    }
    sayHi(name: string): string {
        return `hello, ${name} my Name is ${this.fullName()}`
    }
}

function makeUser(user: User05){
    return "hi~!"
}

// interface로 형상화해놓은 내용에 데이터를 구체화 시킴
makeUser({
    firstName: "nico",
    lastName: "las",
    fullName: () => "xx",
    sayHi: (name) => `hello ${name}`
})


////////////
type PlayerA = {
    name : string
}

type PlayerAA = PlayerA & {
    lastName: string
}

const playerA : PlayerAA = {
    name: 'nico',
    lastName: 'xxx'
}

////////////

interface PlayerB {
    name: string
}

interface PlayerBB extends PlayerB {
    lastName: string
}

// interface는 같은 이름끼리 합쳐지는 성질이 있다. (type은 불가)
interface PlayerBB {
    health: number
}
const playerB : PlayerBB = {
    name: 'nico',
    lastName: 'lass',
    health: 10
}

/////////
type PlayerC = {
    firstName: string
}

interface PlayerD {
    firstName: string
}

class User06 implements PlayerD { // PlayerC도 가능! 둘이 비슷하다!
    constructor(
        public firstName: string
    ){}
}

// 정리: 클래스나 Object의 모양을 정의하고 싶으면 interface를 사용, 그 외에는 Type을 사용해라~