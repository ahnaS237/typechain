// call signature
type SuperPrint = {
  (arr: number[]): void;
  (arr: boolean[]): void;
  (arr: (number | boolean)[]): void;
  (arr: any[]): void;
};

const superPrint: SuperPrint = (arr) => {
  arr.forEach((i) => console.log(i));
};

superPrint([1, 2, 3, 4]);
superPrint([true, false, false, true]);
superPrint([1, 2, false, true]);
superPrint([1, "f", false]);

// call signature
// type SuperPrint2 = {
//     // generic class option : TypePlaceHolder
//     //<TypePlaceHolder>(arr: TypePlaceHolder[]): void;
//     <T>(arr: T[]): T;
// };

// generic
type SuperPrint2 = <T>(arr: T[]) => T;

// const superPrint2: SuperPrint = (arr) => {
//     arr.forEach((i) => console.log(i));
// };

const superPrint2: SuperPrint2 = (arr) => arr[0];

var aa = superPrint2([1, 2, 3, 4]);
var bb = superPrint2([true, false, false, true]);
var cc = superPrint2([1, 2, false, true]);
var dd = superPrint2([1, "f", false]);

// generic palceHolder multiple argument
type SuperPrint3 = <T, M>(arr: T, arr2: M) => T;
const superPrint3: SuperPrint3 = (arr) => arr[0];

superPrint3([1, 2], "d");
superPrint3([1, false], 1);

// function Generic
function superPrint4<T>(arr: T[]) {
  return arr[0];
}
// arrow function Generic
const superPrint5 = <T>(arr: T[]) => {
  return arr[0];
};

// 둘다 가능
superPrint5(["a", "b"]);
superPrint5<string>(["a", "b"]);

// 제네릭 타입을 전달받아서 설정함 
// 범위가 넓은 타입의 변수를 사용해야 될때 제네릭을 사용하면 좋다! : 자료형을 지정해 줄 수 있으니까
type Player<T> = {
  name: string;
  extraInfo: T;
};

type NicoExtra = { favFood : string }
type NocoPlayer = Player<NicoExtra>

const noci: NocoPlayer = {
    name: "nico",
    extraInfo: {
        favFood: "김치"
    }
}

// type 설정된 내용으로 바로 사용도 가능
const lynn: Player<null> = {
    name: "lynn",
    extraInfo: null
}
