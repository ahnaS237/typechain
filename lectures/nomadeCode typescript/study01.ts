// 파라미터 타입으로 여러개를 정의할 경우 (다형성 정의)
type sturdy10_Add = {
  (a: number, b: number): number;
  (a: number, b: string): number;
};

// arrow Function
const sturdy10_add: sturdy10_Add = (a, b) => {
  if (typeof b === "string") return a;
  return a + b;
};

// 파라미터 갯수가 다를 경우
type sturdy10_Add2 = {
  (a: number, b: number): number;
  (a: number, b: number, c: number): number;
};

//  c?: number 로 optional하게 세팅 (arrow Function)
const sturdy10_add2: sturdy10_Add2 = (a, b, c?: number) => {  
  if (c) return a + b + c;
  return a + b;
};

sturdy10_add2(1,2)
sturdy10_add2(1,2,3)

