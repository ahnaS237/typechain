const superPrint = (arr) => {
    arr.forEach((i) => console.log(i));
};
superPrint([1, 2, 3, 4]);
superPrint([true, false, false, true]);
superPrint([1, 2, false, true]);
superPrint([1, "f", false]);
// const superPrint2: SuperPrint = (arr) => {
//     arr.forEach((i) => console.log(i));
// };
const superPrint2 = (arr) => arr[0];
var aa = superPrint2([1, 2, 3, 4]);
var bb = superPrint2([true, false, false, true]);
var cc = superPrint2([1, 2, false, true]);
var dd = superPrint2([1, "f", false]);
const superPrint3 = (arr) => arr[0];
superPrint3([1, 2], "d");
superPrint3([1, false], 1);
// function Generic
function superPrint4(arr) {
    return arr[0];
}
// arrow function Generic
const superPrint5 = (arr) => {
    return arr[0];
};
// 둘다 가능
superPrint5(["a", "b"]);
superPrint5(["a", "b"]);
const noci = {
    name: "nico",
    extraInfo: {
        favFood: "김치"
    }
};
// type 설정된 내용으로 바로 사용도 가능
const lynn = {
    name: "lynn",
    extraInfo: null
};
