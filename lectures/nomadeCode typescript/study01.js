// arrow Function
const sturdy10_add = (a, b) => {
    if (typeof b === "string")
        return a;
    return a + b;
};
//  c?: number 로 optional하게 세팅 (arrow Function)
const sturdy10_add2 = (a, b, c) => {
    if (c)
        return a + b + c;
    return a + b;
};
sturdy10_add2(1, 2);
sturdy10_add2(1, 2, 3);
