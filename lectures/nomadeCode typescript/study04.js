class User04 {
    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
class Player04 extends User04 {
    fullName() {
        return `${this.firstName} ${this.lastName}`;
    }
    sayHi(name) {
        return `hello, ${name} my Name is ${this.fullName()}`;
    }
}
// 클래스에 특정 메소드나 property를 상속
class Player05 {
    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    fullName() {
        return `${this.firstName} ${this.lastName}`;
    }
    sayHi(name) {
        return `hello, ${name} my Name is ${this.fullName()}`;
    }
}
function makeUser(user) {
    return "hi~!";
}
// interface로 형상화해놓은 내용에 데이터를 구체화 시킴
makeUser({
    firstName: "nico",
    lastName: "las",
    fullName: () => "xx",
    sayHi: (name) => `hello ${name}`
});
const playerA = {
    name: 'nico',
    lastName: 'xxx'
};
const playerB = {
    name: 'nico',
    lastName: 'lass',
    health: 10
};
class User06 {
    constructor(firstName) {
        this.firstName = firstName;
    }
}
// 정리: 클래스나 Object의 모양을 정의하고 싶으면 interface를 사용, 그 외에는 Type을 사용해라~
