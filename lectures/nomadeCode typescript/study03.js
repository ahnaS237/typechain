// class 사용하기
class Human {
    constructor(firstName, lastName, nickName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickName = nickName;
    }
    getFullName() {
        //console.log(`${this.firstName} ${this.lastName}`)
        return `${this.firstName} ${this.lastName}`;
    }
}
class Student extends Human {
    //constructor( 
    // private firstName: string,
    // private lastName: string,
    // public nickName: string
    //){}
    getNickName() {
        return `${this.nickName}`;
    }
}
const nico = new Student("nico", "las", "니꼬");
//console.log(nico.nickName);
let fullName = nico.getFullName();
let nickName = nico.getNickName();
console.log(fullName, ' ', nickName);
class Dict {
    constructor() {
        this.words = {}; // 초기화
    }
    add(word) {
        if (this.words[word.term] === undefined) {
            this.words[word.term] = word.def; // term을 key로 def를 value로 넣는다
        }
    }
    def(term) {
        return this.words[term]; // words에 저장된 값들 중에서 term로 값을 찾아서 return
    }
    static hello() {
        console.log('hello ~');
    }
}
class Word {
    constructor(term, def) {
        this.term = term;
        this.def = def;
    }
}
// 추가할 사전 단어
const kimchi = new Word("kimchi", "한국의 음식"); // 여기서만 값 입력가능 (readonly:수정불가)
const dict = new Dict();
dict.add(kimchi); // 추가한 사전 (단어)
let result_dict = dict.def("kimchi"); // 찾는 단어
console.log(result_dict);
